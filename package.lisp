;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.
(in-package #:cl-l10n.system)

(defpackage #:cl-l10n 
  (:use #:cl #:cl-ppcre #:flexi-streams #:cl-fad #:iterate)
  (:shadow cl:format cl:formatter)
  (:shadowing-import-from :cl-fad
                          #:copy-stream #:copy-file)
  (:export #:locale-name
           #:category-name
           #:locale
           #:current-locale
           #:category
           #:locale-error
           #:get-category
           #:locale-value
           #:load-all-locales
           #:normalize-locale-list
           #:get-locale
           #:set-locale
           #:with-locale
           #:*locale-path*
           #:*locale*
           #:load-default-locale
           #:parser-error
           
           #:format-number
           #:print-number
           #:print-number-to-string
           #:format-money
           #:print-money
           #:print-money-to-string
           #:format-time
           #:print-time
           #:parse-number

           #:*float-digits*
           #:shadow-format
           #:parse-time

           #:reload-resources
           
           #:capitalize-first-letter
           #:capitalize-first-letter!

           #:lookup-resource
           #:lookup-resource-without-fallback
           #:localize
           #:resource-missing
           #:defresources
           #:enable-sharpquote-reader
           #:with-sharpquote-reader
           #:lookup-first-matching-resource

           #:consonantp
           #:vowelp
           #:high-vowel-p
           #:low-vowel-p
           #:last-vowel-of
           #:starts-with-consonant-p
           #:starts-with-vowel-p
           
           #:english-plural-of
           #:english-indefinite-article-for
           #:hungarian-definite-article-for
           #:hungarian-plural-of
           ))

           ;; attila: these symbols are very frequent and cause a lot of headaches
           ;; when integrating cl-l10n into other projects. they should
           ;; be renamed to *p and *-p if we really want to export them
           ;;#:month #:day #:year #:hour #:minute #:second
           ;;#:date-divider #:time-divider #:weekday #:noon-midn
           ;;#:secondp #:am-pm #:zone

(defpackage #:cl-l10n.lang
  (:use :common-lisp :cl-l10n)

  (:shadowing-import-from :cl-l10n
   #:defresources)

  (:export

   ;; <numbers> / <symbols>
   #:number-symbol

   #:plural-of

   #:decimal
   #:percent-sign
   #:native-zero-digit
   #:pattern-digit
   #:plus-sign
   #:minus-sign
   #:exponential
   #:per-mille
   #:infinity
   #:nan

   #:currency-symbol
   #:currency-name
   #:language
   #:script
   #:territory
   #:variant

   #:with-indefinite-article
   #:with-definite-article

   #:today
   #:month
   #:day
   #:quarter

   #:january
   #:february
   #:marc
   #:april
   #:may
   #:june
   #:july
   #:august
   #:september
   #:october
   #:november
   #:december

   #:first-quarter
   #:second-quarter
   #:third-quarter
   #:fourth-quarter

   #:sunday
   #:monday
   #:tuesday
   #:wednesday
   #:thursday
   #:friday
   #:saturday
   ))
