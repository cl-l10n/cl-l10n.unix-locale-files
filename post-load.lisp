;;; -*- Mode: LISP; Syntax: ANSI-Common-Lisp; Base: 10 -*-
;; See the file LICENCE for licence information.
(in-package :cl-l10n)

(eval-when (:compile-toplevel :load-toplevel :execute)
  ;; this loads the 'en resouces, which in turn export some symbols. so do
  ;; this at compile-time, too. TODO this should be handled by the locale fallback...
  (load-resource "en")
  (load-default-locale))
